package com.parelli.IRCReader;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.event.ChangeEvent;

import org.schwering.irc.lib.IRCConnection;
import org.schwering.irc.lib.IRCEventAdapter;
import org.schwering.irc.lib.IRCEventListener;
import org.schwering.irc.lib.IRCModeParser;
import org.schwering.irc.lib.IRCUser;
import org.schwering.irc.lib.IRCUtil;

public class ChatUI extends IRCEventAdapter implements IRCEventListener {

	private static final long serialVersionUID = -7836541552235153032L;
	
	private IRCConnection conn;
	private String target = null;
	private String myNick = null;
	private String server = null;
	private int portlow = 0;
	private int porthigh = 0;
	private int slottime = 10000;

	public ChatUI(String _target, String _myNick, String _server, int _portlow, int _porthigh) {
		target = _target;
		myNick = _myNick;
		server  = _server;
		portlow  = _portlow;
		porthigh  = _porthigh;
		start();
	}

	public void connect() throws IOException {
		conn = new IRCConnection(
				server, 
				portlow, 
				porthigh, 
				null, 
				myNick, 
				myNick, 
				myNick);
		conn.addIRCEventListener(this);
		conn.setPong(false);
		conn.setDaemon(false);
		conn.setColors(true);
		conn.connect();
	}
	
    public String getChannelName() {
        return target;
    }
   
    // sends a chat message to the server
    public void send(String msg) {
    	System.out.println("Sending msg: " + msg);
    }
    
    // set the IRC connection for this object
    public void setIRCConnection(IRCConnection conn) {
    	System.out.println("setting connection object");
    	this.conn = conn;
    }
    
   
    public void doPong(String arg0) {
		System.out.println("PONG: " + arg0);
		conn.doPong(arg0);
	}

	public void stateChanged(ChangeEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void onRegistered() {
		// TODO Auto-generated method stub
		
	}

	public void onDisconnected() {
		// TODO Auto-generated method stub
		
	}

	public void onError(String msg) {
		// TODO Auto-generated method stub
		
	}

	public void onError(int num, String msg) {
		// TODO Auto-generated method stub
		
	}

	public void onInvite(String chan, IRCUser user, String passiveNick) {
		// TODO Auto-generated method stub
		
	}

	public void onJoin(String chan, IRCUser user) {
		//System.out.println("User: " + user + " has joined " + chan);
		// TODO Auto-generated method stub
		
	}

	public void onKick(String chan, IRCUser user, String passiveNick, String msg) {
		// TODO Auto-generated method stub
		
	}

	public void onMode(String chan, IRCUser user, IRCModeParser modeParser) {
		// TODO Auto-generated method stub
		
	}

	public void onMode(IRCUser user, String passiveNick, String mode) {
		// TODO Auto-generated method stub
		
	}

	public void onNick(IRCUser user, String newNick) {
		// TODO Auto-generated method stub
		System.out.println("Notice: " + user + " newnick: " + newNick);
	}

	public void onNotice(String target, IRCUser user, String msg) {
		// TODO Auto-generated method stub
		
	}

	public void onPart(String chan, IRCUser user, String msg) {
		//System.out.println("User: " + user + " has left " + chan + " saying: " + msg);
		// TODO Auto-generated method stub
		
	}

	public void onPing(String ping) {
		//System.out.println("PING: " + ping);
		doPong(ping);
	}
	
	public void onPrivmsg(String target, IRCUser user, String msg) {
		//System.out.println("Target: " + target + " User: " + user + " Msg: " +  msg);
		System.out.println("Target: " + target + " User: " + user + " Msg: " +  msg);
		//String newmsg = IRCUtil.parseColors(msg);
		
		//conn.doPrivmsg("Nilo", "\1Test\1");
		// TODO Auto-generated method stub
		
	}

	public void onQuit(IRCUser user, String msg) {
		//System.out.println("User: " + user + " has quit IRC");
		// TODO Auto-generated method stub
		
	}

	public void onReply(int num, String value, String msg) {
		// TODO Auto-generated method stub
		
	}

	public void onTopic(String chan, IRCUser user, String topic) {
		
		// TODO Auto-generated method stub
		
	}

	public void unknown(String prefix, String command, String middle, String trailing) {
		System.out.println("Prefix: " + prefix + " Command: " + command + " Middle: " + middle + " Trailing: " + trailing);
		// TODO Auto-generated method stub
		
	}
	
	public void start() {
		try {
			connect();
		} catch (IOException e) {
			System.out.println("Unable to connect to chat server");
			e.printStackTrace();
		}
		conn.doJoin(target);
	}
	
	public void stop() {
		conn.doQuit();
		conn.close();
	}

} 
