package com.parelli.IRCReader;

public class IRCReader {

	private static final long serialVersionUID = 3813048127999101353L;
	public final static String IRC_SERVER = "irc.phazenet.com";
	public final static int IRC_PORT_LOW = 6667; 
	public final static int IRC_PORT_HIGH = 6669; 
	public final static String IRC_CHANNEL = "#audiobooks";

	public static void main(String[] args) {
		ChatUI cu = new ChatUI(IRC_CHANNEL, "Nilo-", IRC_SERVER, IRC_PORT_LOW, IRC_PORT_HIGH);
	}
}
